﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassicWarcraftMagicImmuneMaskGenerator.Annotations;

namespace ClassicWarcraftMagicImmuneMaskGenerator
{
	class MainWindowViewModel : INotifyPropertyChanged
	{
		private ObservableCollection<ImmunityCheckBox> immunity = new ObservableCollection<ImmunityCheckBox>
		{
			new ImmunityCheckBox {Name = "Charm", Value = 1},
			new ImmunityCheckBox {Name = "Disoriented", Value = 2},
			new ImmunityCheckBox {Name = "Disarm", Value = 4},
			new ImmunityCheckBox {Name = "Distract", Value = 8},
			new ImmunityCheckBox {Name = "Fear", Value = 16},
			new ImmunityCheckBox {Name = "Grip", Value = 32},
			new ImmunityCheckBox {Name = "Root", Value = 64},
			new ImmunityCheckBox {Name = "Pacify", Value = 128},
			new ImmunityCheckBox {Name = "Silence", Value = 256},
			new ImmunityCheckBox {Name = "Sleep", Value = 512},
			new ImmunityCheckBox {Name = "Snare", Value = 1024},
			new ImmunityCheckBox {Name = "Stun", Value = 2048},
			new ImmunityCheckBox {Name = "Freeze", Value = 4096},
			new ImmunityCheckBox {Name = "Knockout", Value = 8192},
			new ImmunityCheckBox {Name = "Bleed", Value = 16384},
			new ImmunityCheckBox {Name = "Bandage", Value = 32768},
			new ImmunityCheckBox {Name = "Polymorph", Value = 65536},
			new ImmunityCheckBox {Name = "Banish", Value = 131072},
			new ImmunityCheckBox {Name = "Shield", Value = 262144},
			new ImmunityCheckBox {Name = "Shackle", Value = 524288},
			new ImmunityCheckBox {Name = "Mount", Value = 1048576},
			new ImmunityCheckBox {Name = "Infected", Value = 2097152},
			new ImmunityCheckBox {Name = "Turn", Value = 4194304},
			new ImmunityCheckBox {Name = "Horror", Value = 8388608},
			new ImmunityCheckBox {Name = "Invulnerability", Value = 16777216},
			new ImmunityCheckBox {Name = "Interrupt", Value = 33554432},
			new ImmunityCheckBox {Name = "Daze", Value = 67108864},
			new ImmunityCheckBox {Name = "Discovery", Value = 134217728},
			new ImmunityCheckBox {Name = "Immune Shield", Value = 268435456},
			new ImmunityCheckBox {Name = "Spapped", Value = 536870912},
			new ImmunityCheckBox {Name = "Enrage", Value = 1073741824}
		};
		public ObservableCollection<ImmunityCheckBox> Immunity { get { return immunity; } set { immunity = value; OnPropertyChanged(); } }

		private bool updating = false;

		private uint immunityValue = 0;

		public uint ImmunityValue
		{
			get
			{
				return immunityValue;
			}
			set
			{
				immunityValue = value;
				uint tempTmmunityValue = value;
                OnPropertyChanged();
				updating = true;
				foreach (ImmunityCheckBox immunityCheckBox in Immunity)
				{
					immunityCheckBox.IsChecked = false;
				}
				updating = false;
				List<uint> myInts = new List<uint> { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824 };
				myInts.Reverse();
				List<uint> foundInts = new List<uint>();
				foreach (uint myInt in myInts)
				{
					if (tempTmmunityValue >= myInt)
					{
						foundInts.Add(myInt);
						tempTmmunityValue -= myInt;
					}
				}

				foreach (uint foundInt in foundInts)
				{
					updating = true;
					foreach (ImmunityCheckBox immunityCheckBox in Immunity)
					{
						if (immunityCheckBox.Value == foundInt)
							immunityCheckBox.IsChecked = true;
					}
					updating = false;
				}
			}
		}

		public MainWindowViewModel()
		{
			foreach (ImmunityCheckBox immunityCheckBox in Immunity)
			{
				immunityCheckBox.OnImunityChanged += (sender, args) =>
				{
					if(!updating)
					{
						immunityValue = 0;
						if (args.Checked)
							immunityValue += args.Value;
						else
							immunityValue -= args.Value;
						OnPropertyChanged(nameof(ImmunityValue));
					}
				};
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}
}
