﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using ClassicWarcraftMagicImmuneMaskGenerator.Annotations;

namespace ClassicWarcraftMagicImmuneMaskGenerator
{
	class ImmunityCheckBox : INotifyPropertyChanged
	{
		public event ImmunityCheckBoxChanged OnImunityChanged;
		public string Name { get; set; }
		public uint Value { get; set; }

		private bool isChecked;
		public bool IsChecked
		{
			get { return isChecked; }
			set
			{
				isChecked = value;
				OnPropertyChanged();
				ImmunityCheckBoxChangedEventArgs e = new ImmunityCheckBoxChangedEventArgs { Checked = value, Value = Value };
				OnImunityChanged?.Invoke(this, e);
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		[NotifyPropertyChangedInvocator]
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
	}

	public delegate void ImmunityCheckBoxChanged(object sender, ImmunityCheckBoxChangedEventArgs e);

	public class ImmunityCheckBoxChangedEventArgs : EventArgs
	{
		public uint Value;
		public bool Checked;
	}
}
